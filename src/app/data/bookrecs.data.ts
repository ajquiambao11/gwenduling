import { BookRec } from '../models/books.model';

export const bookrecs: BookRec[] = [
  {
    title: 'Everything I Never Told You',
    author: 'Celeste Ng',
    image: 'everything_I_never_told_you.jpg',
    genre: '#contemporary #historicalfiction #family',
    link: 'https://fullybooked.sjv.io/P0AE9X',
  },
  {
    title: 'Know My Name',
    author: 'Chanel Miller',
    image: 'know_my_name.jpg',
    genre: '#memoir #feminism',
    link: 'https://fullybooked.sjv.io/BXGaW9',
  },
  {
    title: 'Educated',
    author: 'Tara Westover',
    image: 'educated.jpg',
    genre: '#memoir #trauma #family',
    link: 'https://fullybooked.sjv.io/n1AxOx',
  },
  {
    title: 'The Heart Principle',
    author: 'Helen Hoang',
    image: 'the_heart_principle.jpg',
    genre: '#romance #family',
    link: 'https://fullybooked.sjv.io/4eL0k1',
  },
  {
    title: 'The Four Winds',
    author: 'Kristin Hannah',
    image: 'the_four_winds.jpeg',
    genre: '#historicalfiction #contemporary #family',
    link: 'https://fullybooked.sjv.io/ORa0XA',
  },
  {
    title: 'Kim Jiyoung, Born 1982',
    author: 'Cho Nam-Joo',
    image: 'kim_jiyoung.jpeg',
    genre: '#feminism',
    link: 'https://fullybooked.sjv.io/4eL031',
  },
  {
    title: 'Wish You Were Here',
    author: 'Jodi Picoult',
    image: 'wish_you_were_here.jpeg',
    genre: '#covid19 #contemporary #realisticfiction',
    link: 'https://fullybooked.sjv.io/KePmVa',
  },
  {
    title: 'Honey Girl',
    author: 'Morgan Rogers',
    image: 'honey_girl.jpg',
    genre: '#contemporary #romance #comingofage',
    link: 'https://fullybooked.sjv.io/BXeMN9',
  },
];

export const cvData = {
  intro: {
    name: 'Rozzette Giorelle D. Argao',
    title: 'Frontend Developer',
    website: 'www.gwenduling.com',
    bio:
      'Hello 🐣, I am a frontend developer and I create web applications with the use of JavaScript',
  },
  personal: {
    birthday: 'March 6, 1996',
    address: 'San Fernando, Pampanga 🇵🇭',
    email: 'gioweeargao@gmail.com',
    mobile: '+639309586986',
  },
  education: [
    {
      school: 'Holy Angel University',
      date: '2008 - 2012',
      content: ['High School'],
    },
    {
      school: 'Holy Angel University',
      date: '2012 - 2016',
      content: [
        'BS in Information Technology major in Web Development',
        'Graduated Magna Cum Laude',
        'Best Thesis (2016)',
        'Outstanding Graduating Web Student (2016)',
        'Outstanding Practicum Web Student (2016)',
        'Outstanding Third Year Web Student (2015)',
      ],
    },
  ],
  work: [
    {
      company: 'TORO Philippines',
      link: 'http://www.torocloud.com',
      titles: [
        {
          title: 'Frontend Intern',
          date: 'June - October 2015',
        },
      ],
    },
    {
      company: 'TORO Philippines',
      link: 'http://www.torocloud.com',
      titles: [
        // {
        //   title: 'Frontend Intern',
        //   date: 'June - October 2015',
        // },
        // {
        //   title: 'Associate Frontend Developer',
        //   date: 'April 2016 - April 2018',
        // },
        // {
        //   title: 'Frontend Developer',
        //   date: 'April 2018 - April 2019',
        // },
        // {
        //   title: 'Lead Frontend Developer',
        //   date: 'April 2019 - January 2020',
        // },
        {
          title: 'Lead Frontend Developer',
          date: 'April 2016 - January 2020',
        },
      ],
    },
    {
      company: 'Owens Asia',
      link: 'https://www.owensasia.com/',
      titles: [
        {
          title: 'Lead Software Engineer',
          date: 'February 2020 - October 2021',
        },
      ],
    },
  ],
  conferences: [
    {
      name: 'JSConf Asia Singapore 2018',
      link: 'https://2018.jsconf.asia/',
      date: 'January 25 2018 - January 27 2018',
    },
  ],
  responsibilities: [
    {
      for: 'Developer',
      content: [
        'Writes and maintains web applications',
        'Creates solutions and compromises for problems',
        'Ensures quality and scalability of code',
      ],
    },
    {
      for: 'Lead',
      content: [
        'Manages a small team and ensures conflicts are resolved',
        'Makes decisions for the limitation and scope of the project',
        'Represents the team during meetings and discussions',
      ],
    },
  ],
  tools: [
    {
      for: 'Development',
      tools: [
        {
          name: 'HTML, CSS3 and Javascript',
          description: 'Fundamental tools for frontend development',
        },
        {
          name: 'Angular',
          link: 'http://angular.io/',
          description: 'Framework for JavaScript',
        },
        {
          name: 'Angular Universal',
          link: 'https://angular.io/guide/universal',
          description: 'Server Side Rendering for Angular',
        },
        {
          name: 'React',
          link: 'https://reactjs.org/',
          description: 'JavaScript Library',
        },
        {
          name: 'Next.js',
          link: 'https://nextjs.org/',
          description: 'Framework for React',
        },
        {
          name: 'Typescript',
          link: 'https://www.typescriptlang.org/',
          description: 'Typed JavaScript',
        },
        {
          name: 'Consuming APIs',
        },
        {
          name: 'LESS, SASS, SCSS',
          description: 'CSS preprocessors',
        },
        {
          name: 'HTML5 Canvas',
          description: 'For drawing animations and dynamic graphics',
        },
      ],
    },
    {
      for: 'Automation and Testing',
      tools: [
        {
          name: 'Chrome developer tools',
        },
        {
          name: 'Gulp and NPM scripts',
          description: 'Task runner',
        },
        {
          name: 'NPM and Yarn',
          description: 'Package managers',
        },
        {
          name: 'Jest and Jasmine',
          description: 'Unit Testing Framework',
        },
        {
          name: 'Bamboo',
          description: 'Continuous integration',
        },
      ],
    },
  ],
};

import { Rating, Title } from '../models/books.model';

export const titles: Title[] = [
  {
    count: 1,
    title: 'The Invisible Life of Addie LaRue',
    author: [
      {
        name: 'V.E. Schwab',
        link: 'https://www.victoriaschwab.com/',
      },
    ],
    buyLink: 'https://fullybooked.sjv.io/9WZW9E',
    readLink:
      'https://www.goodreads.com/book/show/50623864-the-invisible-life-of-addie-larue',
    rating: Rating.FourStars,
  },
  {
    count: 2,
    title: 'The Nightingale',
    author: [
      {
        name: 'Kristin Hannah',
        link: 'https://kristinhannah.com/',
      },
    ],
    buyLink: 'https://fullybooked.sjv.io/GjkjQ2',
    readLink: 'https://www.goodreads.com/book/show/21853621-the-nightingale',
    rating: Rating.FiveStars,
  },
  {
    count: 3,
    title: 'Night Sky and Exit Wounds',
    author: [
      {
        name: 'Ocean Vuong',
        link: 'https://www.oceanvuong.com/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Night-Sky-with-Exit-Wounds-Ocean-Vuong/9781556594953?ref=grid-view&qid=1650710539833&sr=1-2',
    readLink:
      'https://www.goodreads.com/book/show/23841432-night-sky-with-exit-wounds',
    rating: Rating.ThreeStars,
  },
  {
    count: 4,
    title: 'The Unhoneymooners',
    author: [
      {
        name: 'Christina Lauren',
        link: 'https://christinalaurenbooks.com/',
      },
    ],
    buyLink:
      'https://www.fullybookedonline.com/the-unhoneymooners-paperback-by-christina-lauren.html',
    readLink: 'https://www.goodreads.com/book/show/42201431-the-unhoneymooners',
    rating: Rating.ThreeStars,
  },
  {
    count: 5,
    title: 'The Picture of Dorian Gray',
    author: [
      {
        name: 'Oscar Wilde',
      },
    ],
    buyLink:
      'https://www.fullybookedonline.com/the-picture-of-dorian-gray-collins-classic-paperback-by-oscar-wilde.html',
    readLink:
      'https://www.goodreads.com/book/show/5297.The_Picture_of_Dorian_Gray',
    rating: Rating.FourStars,
  },
  {
    count: 6,
    title: 'Get a Life, Chloe Brown',
    author: [{ name: 'Talia Hibbert', link: 'https://www.taliahibbert.com/' }],
    readLink:
      'https://www.goodreads.com/book/show/43884209-get-a-life-chloe-brown?',
    buyLink:
      'https://www.bookdepository.com/Get-Life-Chloe-Brown-Talia-Hibbert/9780349425214?ref=grid-view&qid=1650710832756&sr=1-1',
    rating: Rating.FourStars,
  },
  {
    count: 7,
    title: 'Severance',
    author: [{ name: 'Ling Ma' }],
    readLink: 'https://www.goodreads.com/book/show/36348525-severance',
    buyLink:
      'https://www.fullybookedonline.com/books-contemporary-fiction-severance-a-novel-paperback-by-ling-ma.html',
    rating: Rating.FourStars,
  },
  {
    count: 8,
    title: 'Know My Name',
    author: [{ name: 'Chanel Miller', link: 'https://chanel-miller.com/' }],
    buyLink: 'https://fullybooked.sjv.io/BXGaW9',
    readLink: 'https://www.goodreads.com/book/show/50196744-know-my-name',
    rating: Rating.FiveStars,
  },
  {
    count: 9,
    title: 'The Truth Project',
    author: [{ name: 'Dante Medema', link: 'https://www.dantemedema.com/' }],
    buyLink: 'https://fullybooked.sjv.io/AoQora',
    readLink: 'https://www.goodreads.com/book/show/50496883-the-truth-project',
    rating: Rating.FiveStars,
  },
  {
    count: 10,
    title: 'And the Mountains Echoed',
    author: [{ name: 'Khaled Housseini', link: 'https://khaledhosseini.com/' }],
    buyLink:
      'https://www.fullybookedonline.com/book-and-the-mountains-echoed-mass-market-khaled-hosseini.html',
    readLink:
      'https://www.goodreads.com/book/show/16115612-and-the-mountains-echoed',
    rating: Rating.ThreeStars,
  },
  {
    count: 11,
    title: 'A Thousand Boy Kisses',
    author: [{ name: 'Tillie Cole', link: 'https://tilliecole.com/' }],
    buyLink:
      'https://www.fullybookedonline.com/book-and-the-mountains-echoed-mass-market-khaled-hosseini.html',
    readLink:
      'https://www.goodreads.com/book/show/25912358-a-thousand-boy-kisses',
    rating: Rating.ThreeStars,
  },
  {
    count: 12,
    title: 'Meet Me in Another Life',
    author: [{ name: 'Catriona Silvey', link: 'http://catrionasilvey.com/' }],
    readLink:
      'https://www.goodreads.com/book/show/54698696-meet-me-in-another-life',
    buyLink:
      'https://www.bookdepository.com/Meet-Me-Another-Life-Catriona-Silvey/9780008399856?ref=grid-view&qid=1650711506181&sr=1-1',
    rating: Rating.ThreeStars,
  },
  {
    count: 13,
    title: 'The Bromance Book Club',
    author: [
      { name: 'Lyssa Kay Adams', link: 'https://www.lyssakayadams.com/' },
    ],
    readLink:
      'https://www.goodreads.com/book/show/44019067-the-bromance-book-club',
    buyLink: 'https://fullybooked.sjv.io/dovox2',
    rating: Rating.FiveStars,
  },
  {
    count: 14,
    title: 'Honey Girl',
    author: [{ name: 'Morgan Rogers' }],
    readLink:
      'https://www.goodreads.com/book/show/49362138-honey-girl?from_search=true&from_srp=true&qid=ZufW4nVMkb&rank=1',
    buyLink: 'https://fullybooked.sjv.io/BXeMN9',
    rating: Rating.FiveStars,
  },
  {
    count: 0,
    title: 'Luck of the Titanic',
    author: [{ name: 'Stacey Lee', link: 'https://www.staceyhlee.com/' }],
    readLink:
      'https://www.goodreads.com/book/show/54711210-luck-of-the-titanic?from_search=true&from_srp=true&qid=wZbfnMX5EQ&rank=1',
    buyLink: 'https://www.bookdepository.com/Luck-of-the-Titanic/9780349431451',
    rating: Rating.DNF,
  },
  {
    count: 15,
    title: 'Aphrodite Made Me Do It',
    author: [
      { name: 'Trista Mateer', link: 'https://www.tristamateerpoetry.com/' },
    ],
    readLink:
      'https://www.goodreads.com/book/show/43819413-aphrodite-made-me-do-it',
    buyLink:
      'https://www.bookdepository.com/Aphrodite-Made-Me-Do-It/9781771681742',
    rating: Rating.FourStars,
  },
  {
    count: 16,
    title: "You've Reached Sam",
    author: [{ name: 'Dustin Thao', link: 'https://www.dustinthao.com/' }],
    readLink: 'https://www.goodreads.com/book/show/53086843-you-ve-reached-sam',
    buyLink:
      'https://www.fullybookedonline.com/you-ve-reached-sam-a-novel-international-edition-paperback-by-dustin-thao.html',
    rating: Rating.TwoStars,
  },
  {
    count: 17,
    title: 'Beautiful Country',
    author: [
      { name: 'Qian Julie Wang', link: 'https://www.qianjuliewang.com/' },
    ],
    buyLink:
      'https://www.fullybookedonline.com/beautiful-country-a-memoir-international-edition-paperback-by-qian-julie-wang.html',
    readLink: 'https://www.goodreads.com/en/book/show/56461570',
    rating: Rating.ThreeStars,
  },
  {
    count: 18,
    title: 'A Lot Like Adios',
    author: [
      {
        name: 'Alexia Daria',
        link: 'https://alexisdaria.com/',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/56197428-a-lot-like-adi-s',
    buyLink:
      'https://www.bookdepository.com/Lot-Like-Adios-Alexis-Daria/9780062959966?ref=grid-view&qid=1650715132361&sr=1-1',
    rating: Rating.FourStars,
  },
  {
    count: 19,
    title: 'The Goldfinch',
    author: [
      { name: 'Donna Tartt', problematic: 'Racism (with use of slurs)' },
    ],
    buyLink:
      'https://www.fullybookedonline.com/book-fiction-the-goldfinch-a-novel-mass-market-by-donna-tartt.html',
    readLink: 'https://www.goodreads.com/book/show/17333223-the-goldfinch',
    rating: Rating.FourStars,
  },
  {
    count: 20,
    title: 'Punching the Air',
    author: [
      {
        name: 'Ibi Zoboi',
        link: 'https://www.ibizoboi.net/',
      },
      {
        name: 'Dr. Yusef Salaam',
        link: 'https://www.yusefspeaks.com/',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/49151299-punching-the-air',
    buyLink:
      'https://www.bookdepository.com/Punching-Air-Ibi-Zoboi/9780008422141?ref=grid-view&qid=1650715569215&sr=1-1',
    rating: Rating.FourStars,
  },
  {
    count: 21,
    title: 'Daughters of Sparts',
    author: [
      {
        name: 'Claire Heywood',
      },
    ],
    readLink:
      'https://www.goodreads.com/book/show/57423631-daughters-of-sparta?from_search=true&from_srp=true&qid=0F2KuByjYa&rank=2',
    buyLink:
      'https://www.fullybookedonline.com/book-daughters-of-sparta-a-novel-paperback-by-claire-heywood.html',
    rating: Rating.FourStars,
  },
  {
    count: 22,
    title: 'Trick Mirror: Reflections on Self Delusion',
    author: [{ name: 'Jia Tolentino' }],
    readLink:
      'https://www.goodreads.com/book/show/43126457-trick-mirror?from_search=true&from_srp=true&qid=a5mI8xLELZ&rank=1',
    buyLink:
      'https://www.fullybookedonline.com/book-non-fiction-essays-trick-mirror-reflections-on-self-delusion-paperback.html',
    rating: Rating.FourStars,
  },
  {
    count: 23,
    title: 'Beach Read',
    author: [{ name: 'Emily Henry', link: 'https://www.emilyhenrybooks.com/' }],
    readLink:
      'https://www.goodreads.com/book/show/52867387-beach-read?from_search=true&from_srp=true&qid=lKnyGiGSro&rank=1',
    buyLink: 'https://fullybooked.sjv.io/VyKyoM',
    rating: Rating.FiveStars,
  },
  {
    count: 24,
    title: 'My Policeman',
    author: [
      {
        name: 'Bethan Roberts',
        link: 'https://bethanrobertswriter.co.uk/',
      },
    ],
    readLink:
      'https://www.goodreads.com/book/show/13153370-my-policeman?from_search=true&from_srp=true&qid=pyl7eiiJyS&rank=1',
    buyLink:
      'https://www.fullybookedonline.com/my-policeman-a-novel-paperback-by-bethan-roberts.html',
    rating: Rating.FourStars,
  },
  {
    count: 25,
    title: 'The Handmaids Tale',
    author: [
      {
        name: 'Margaret Atwood',
        link: 'https://margaretatwood.ca/',
        problematic:
          'Supported a man accused of SA, exploited the struggles of POC and made it white',
      },
    ],
    readLink:
      'https://www.goodreads.com/book/show/38447.The_Handmaid_s_Tale?from_search=true&from_srp=true&qid=rjfnmViZDQ&rank=1',
    buyLink:
      'https://www.fullybookedonline.com/the-handmaid-s-tale-paperback-by-margaret-atwood.html',
    rating: Rating.ThreeStars,
  },
  {
    count: 26,
    title: 'The Memory Police',
    author: [
      {
        name: 'Yoko Ogawa',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/37004370-the-memory-police',
    buyLink:
      'https://www.fullybookedonline.com/book-fiction-memory-police-paperback-by-yoko-ogawa.html',
    rating: Rating.FourStars,
  },
  {
    count: 27,
    title: 'Born a Crime: Stories From a South African Childhood',
    author: [
      {
        name: 'Trevor Noah',
        link: 'https://www.trevornoah.com/',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/29780253-born-a-crime',
    buyLink: 'https://fullybooked.sjv.io/QOXORx',
    rating: Rating.FiveStars,
  },
  {
    count: 28,
    title: 'The Color Purple',
    author: [
      {
        name: 'Alice Walker',
        link: 'https://alicewalkersgarden.com/',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/52892857-the-color-purple',
    buyLink:
      'https://www.bookdepository.com/Color-Purple-Alice-Walker/9781474607254?ref=grid-view&qid=1650717396074&sr=1-1',
    rating: Rating.ThreeStars,
  },
  {
    count: 29,
    title: 'The Great Alone',
    author: [
      {
        name: 'Kristin Hannah',
        link: 'https://kristinhannah.com/',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/34912895-the-great-alone',
    buyLink:
      'https://www.bookdepository.com/Great-Alone-Kristin-Hannah/9781447286035?ref=grid-view&qid=1650718400389&sr=1-1',
    rating: Rating.FourStars,
  },
  {
    count: 30,
    title: "Josh and Hazel's Guid to Not Dating",
    author: [
      {
        name: 'Christina Lauren',
        link: 'https://christinalaurenbooks.com/',
      },
    ],
    buyLink: 'https://https://fullybooked.sjv.io/yRv2gv',
    readLink:
      'https://www.goodreads.com/book/show/40189670-josh-and-hazel-s-guide-to-not-dating',
    rating: Rating.FiveStars,
  },
  {
    count: 31,
    title: "America's First Daughter",
    author: [
      {
        name: 'Stephanie Dray',
        link: 'https://www.stephaniedray.com/',
      },
      {
        name: 'Laura Kamoie',
        link: 'https://www.laurakamoie.com/',
      },
    ],
    buyLink:
      'https://www.fullybookedonline.com/book-historical-america-s-first-daughter-paperback-by-stephanie-dray-and-laura-kamoie.html',
    readLink:
      'https://www.goodreads.com/book/show/25817162-america-s-first-daughter',
    rating: Rating.FourStars,
  },
  {
    count: 32,
    title: 'Bringing Down The Duke',
    author: [
      {
        name: 'Evie Dunmore',
        link: 'http://eviedunmore.com/',
      },
    ],
    buyLink:
      'https://www.fullybookedonline.com/book-fiction-bringing-down-the-duke-paperback-by-evie-dunmore.html',
    readLink:
      'https://www.goodreads.com/book/show/43521785-bringing-down-the-duke',
    rating: Rating.ThreeStars,
  },
  {
    count: 33,
    title: 'The Fine Print',
    author: [
      {
        name: 'Lauren Asher',
        link: 'https://laurenasher.com/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Fine-Print-Lauren-Asher/9780349433448?ref=grid-view&qid=1650719136390&sr=1-1',
    readLink: 'https://www.goodreads.com/book/show/57658071-the-fine-print',
    rating: Rating.FourStars,
  },
  {
    count: 34,
    title: 'Untamed',
    author: [
      {
        name: 'Glennon Doyle',
      },
    ],
    buyLink:
      'https://www.fullybookedonline.com/book-humanities-untamed-hardcover-by-glennon-doyle.html',
    readLink: 'https://www.goodreads.com/book/show/52129515-untamed',
    rating: Rating.FourStars,
  },
  {
    count: 35,
    title: 'The Simple Wild',
    author: [
      {
        name: 'K.A. Tucker',
        link: 'https://www.katuckerbooks.com/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Simple-Wild-K-Tucker/9781501133435?ref=grid-view&qid=1650719286843&sr=1-1',
    readLink: 'https://www.goodreads.com/book/show/36373564-the-simple-wild',
    rating: Rating.FourStars,
  },
  {
    count: 36,
    title: 'Wild at Heart',
    author: [
      {
        name: 'K.A. Tucker',
        link: 'https://www.katuckerbooks.com/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Wild-at-Heart-K-Tucker/9781999015428?ref=grid-view&qid=1650719286843&sr=1-2',
    readLink: 'https://www.goodreads.com/book/show/53024322-wild-at-heart',
    rating: Rating.FourStars,
  },
  {
    count: 0,
    title: 'Lolita',
    author: [
      {
        name: 'Vladimir Nabokov',
      },
    ],
    readLink: 'https://www.goodreads.com/book/show/7604.Lolita',
    buyLink:
      'https://www.fullybookedonline.com/lolita-50th-anniversary-edition-paperback-by-vladimir-nabokov.html',
    rating: Rating.DNF,
  },
  {
    count: 37,
    title: 'The Inheritance of Orquidea Divina',
    author: [
      {
        name: 'Zoraida Cordova',
        link: 'https://zoraidacordova.com/',
      },
    ],
    readLink:
      'https://www.goodreads.com/book/show/56898076-the-inheritance-of-orqu-dea-divina',
    buyLink:
      'https://www.fullybookedonline.com/the-inheritance-of-orquidea-divina-a-novel-export-edition-paperback-by-zoraida-cordova.html',
    rating: Rating.ThreeStars,
  },
  {
    count: 38,
    title: 'Alone With You In The Ether',
    author: [
      {
        name: 'Olivie Blake',
        link: 'https://www.olivieblake.com/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Alone-With-You-in-the-Ether/9798655480407',
    readLink:
      'https://www.goodreads.com/book/show/54208009-alone-with-you-in-the-ether',
    rating: Rating.FourStars,
  },
  {
    count: 39,
    title: 'Feminism, Interrupted: Disrupting Power',
    author: [
      {
        name: 'Lola Olufemi',
        link: 'https://lolaolufemi.co.uk/',
      },
    ],
    buyLink:
      'https://www.bookdepository.com/Feminism-Interrupted-Lola-Olufemi/9780745340067?ref=grid-view&qid=1650719653724&sr=1-1',
    readLink:
      'https://www.goodreads.com/book/show/48856308-feminism-interrupted',
    rating: Rating.FourStars,
  },
  {
    count: 40,
    title: 'The Maid',
    author: [
      {
        name: 'Nita Prose',
        link: 'https://www.nitaprose.com/',
      },
    ],
    readLink:
      'https://www.goodreads.com/book/show/55196813-the-maid?from_search=true&from_srp=true&qid=EfwbdEanMY&rank=1',
    buyLink:
      'https://www.fullybookedonline.com/book-crime-mystery-the-maid-a-novel-hardcover-by-nita-prose.html',
    rating: Rating.ThreeStars,
  },
];

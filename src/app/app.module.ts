import { BrowserModule, TransferState } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BlogComponent } from './components/blog/blog.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ButterLinkComponent } from './components/butter-link/butter-link.component';
import { BlogCategoriesComponent } from './components/blog-categories/blog-categories.component';
import { BlogPostComponent } from './components/blog-post/blog-post.component';
import { BlogRecentComponent } from './components/blog-recent/blog-recent.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { UnsupportedBrowserComponent } from './components/unsupported-browser/unsupported-browser.component';
import { ErrorComponent } from './components/error/error.component';
import { CvComponent } from './components/cv/cv.component';
import { InfoComponent } from './components/info/info.component';
import { ConfettiComponent } from './components/confetti/confetti.component';
import { EventComponent } from './components/event/event.component';
import { LinkTreeComponent } from './components/link-tree/link-tree.component';
import { LinkProfileComponent } from './components/link-profile/link-profile.component';
import { LinkBookStoresComponent } from './components/link-book-stores/link-book-stores.component';
import { LinkBookRecComponent } from './components/link-book-rec/link-book-rec.component';
import { LinkListComponent } from './components/link-list/link-list.component';
import { LinkBookAdsComponent } from './components/link-book-ads/link-book-ads.component';
import { BooksComponent } from './components/books/books.component';
import { BookRatingsComponent } from './components/book-ratings/book-ratings.component';
import { BookTitlesComponent } from './components/book-titles/book-titles.component';
import { RatingLegendComponent } from './components/rating-legend/rating-legend.component';
import { TooltipDirective } from './directives/tooltip.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    BlogComponent,
    LoaderComponent,
    ButterLinkComponent,
    BlogCategoriesComponent,
    BlogPostComponent,
    BlogRecentComponent,
    PageNotFoundComponent,
    UnsupportedBrowserComponent,
    ErrorComponent,
    CvComponent,
    InfoComponent,
    ConfettiComponent,
    EventComponent,
    LinkTreeComponent,
    LinkProfileComponent,
    LinkBookStoresComponent,
    LinkBookRecComponent,
    LinkListComponent,
    LinkBookAdsComponent,
    BooksComponent,
    BookRatingsComponent,
    BookTitlesComponent,
    RatingLegendComponent,
    TooltipDirective,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
  ],
  providers: [TransferState],
  bootstrap: [AppComponent],
})
export class AppModule {}

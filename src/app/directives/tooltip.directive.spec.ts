import { TooltipDirective } from './tooltip.directive';

describe('TooltipDirective', () => {
  it('should create an instance', () => {
    const el = {
      nativeElement: document.createElement('div'),
    };
    const directive = new TooltipDirective(el);
    expect(directive).toBeTruthy();
  });
});

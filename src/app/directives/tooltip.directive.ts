import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  OnDestroy,
} from '@angular/core';

@Directive({
  selector: '[appTooltip]',
})
export class TooltipDirective implements OnDestroy {
  @Input() tooltip = '';

  private myPopup: any;
  private timer: any;
  private isOpen = false;

  constructor(private el: ElementRef) {}

  ngOnDestroy(): void {
    if (this.myPopup) {
      this.isOpen = false;
      this.myPopup.remove();
    }
  }

  @HostListener('mouseenter') onMouseEnter(): void {
    if (!this.isOpen) {
      this.setTooltip();
    }
  }

  @HostListener('click') onMouseClick(): void {
    if (this.isOpen) {
      this.removeTooltip();
    } else {
      this.setTooltip();
    }
  }

  @HostListener('mouseleave') onMouseLeave(): void {
    this.removeTooltip();
  }

  setTooltip(): void {
    this.timer = setTimeout(() => {
      const x =
        this.el.nativeElement.getBoundingClientRect().left +
        this.el.nativeElement.offsetWidth / 2;
      const y =
        this.el.nativeElement.getBoundingClientRect().top +
        this.el.nativeElement.offsetHeight +
        6;
      this.createTooltipPopup(x, y);
      this.isOpen = true;
    });
  }

  createTooltipPopup(x: number, y: number): void {
    const popup = document.createElement('div');
    popup.innerHTML = this.tooltip;
    popup.setAttribute('class', 'tooltip-container');
    popup.style.top = y.toString() + 'px';
    popup.style.left = x.toString() + 'px';
    document.body.appendChild(popup);
    this.myPopup = popup;
  }

  removeTooltip(): void {
    this.isOpen = false;
    if (this.timer) {
      clearTimeout(this.timer);
    }
    if (this.myPopup) {
      this.myPopup.remove();
      this.myPopup.style.display = 'none';
    }

    const tooltips = document.querySelectorAll(
      '.tooltip-container'
    ) as unknown as HTMLElement[];
    tooltips.forEach((tooltip: HTMLElement) => {
      tooltip.style.display = 'none';
      tooltip.classList.remove('.tooltip-container');
    });
  }
}

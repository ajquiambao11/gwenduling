import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkBookRecComponent } from './link-book-rec.component';

describe('LinkBookRecComponent', () => {
  let component: LinkBookRecComponent;
  let fixture: ComponentFixture<LinkBookRecComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkBookRecComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkBookRecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

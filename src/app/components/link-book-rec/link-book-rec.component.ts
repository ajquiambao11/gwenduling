import { Component, Input } from '@angular/core';
import { BookRec } from 'src/app/models/books.model';

@Component({
  selector: 'app-link-book-rec',
  templateUrl: './link-book-rec.component.html',
  styleUrls: ['./link-book-rec.component.scss'],
})
export class LinkBookRecComponent {
  @Input() bookrec?: BookRec | undefined;
  @Input() even?: boolean;
  constructor() {}
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { last } from 'rxjs/operators';
import { titles } from 'src/app/data/books.data';

@Component({
  selector: 'app-book-ratings',
  templateUrl: './book-ratings.component.html',
  styleUrls: ['./book-ratings.component.scss'],
})
export class BookRatingsComponent implements OnInit {
  @Input() activeBookIndex = -1;
  @Output() updateActive = new EventEmitter();

  titles = titles;
  lastCount = 0;
  remainingCount = 0;
  constructor() {}

  ngOnInit(): void {
    const bookTitles = this.titles.filter((title) => title.count !== 0);
    this.lastCount = bookTitles[bookTitles.length - 1].count + 1;
    this.remainingCount = 101 - this.lastCount;
  }

  update(index: number): void {
    this.updateActive.emit(index);
  }
}

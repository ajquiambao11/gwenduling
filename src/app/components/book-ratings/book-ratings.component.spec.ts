import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookRatingsComponent } from './book-ratings.component';

describe('BookRatingsComponent', () => {
  let component: BookRatingsComponent;
  let fixture: ComponentFixture<BookRatingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BookRatingsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookRatingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

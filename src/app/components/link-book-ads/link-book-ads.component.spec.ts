import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkBookAdsComponent } from './link-book-ads.component';

describe('LinkBookAdsComponent', () => {
  let component: LinkBookAdsComponent;
  let fixture: ComponentFixture<LinkBookAdsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkBookAdsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkBookAdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { titles } from '../../data/books.data';

@Component({
  selector: 'app-book-titles',
  templateUrl: './book-titles.component.html',
  styleUrls: ['./book-titles.component.scss'],
})
export class BookTitlesComponent implements OnInit {
  @Input() activeBookIndex = -1;
  @Output() updateActive = new EventEmitter();

  titles = titles;
  constructor() {}

  ngOnInit(): void {}

  update(index: number): void {
    this.updateActive.emit(index);
  }
}

import { Component, OnInit } from '@angular/core';
import { booksMeta } from 'src/app/data/meta.data';
import { MetaService } from '../../services/meta.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss'],
})
export class BooksComponent implements OnInit {
  activeBookIndex = -1;
  constructor(private metaService: MetaService) {}

  ngOnInit(): void {
    this.metaService.setMetaTags(booksMeta);
  }

  updateActiveBookIndex(index: number): void {
    this.activeBookIndex = index;
  }
}

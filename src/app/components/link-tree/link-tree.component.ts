import { Component, OnInit } from '@angular/core';
import { bookrecs } from 'src/app/data/bookrecs.data';
import { linkMeta } from 'src/app/data/meta.data';
import { BookRec } from 'src/app/models/books.model';
import { MetaService } from 'src/app/services/meta.service';

@Component({
  selector: 'app-link-tree',
  templateUrl: './link-tree.component.html',
  styleUrls: ['./link-tree.component.scss'],
})
export class LinkTreeComponent implements OnInit {
  bookrecs: BookRec[] = bookrecs;
  constructor(private metaService: MetaService) {}

  ngOnInit(): void {
    this.metaService.setMetaTags(linkMeta);
  }
}

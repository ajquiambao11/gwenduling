import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingLegendComponent } from './rating-legend.component';

describe('RatingLegendComponent', () => {
  let component: RatingLegendComponent;
  let fixture: ComponentFixture<RatingLegendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatingLegendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingLegendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

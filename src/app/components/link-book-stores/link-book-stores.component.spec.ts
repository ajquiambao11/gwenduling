import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkBookStoresComponent } from './link-book-stores.component';

describe('LinkBookStoresComponent', () => {
  let component: LinkBookStoresComponent;
  let fixture: ComponentFixture<LinkBookStoresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinkBookStoresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkBookStoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export interface BookRec {
  title: string;
  author: string;
  genre: string;
  image: string;
  link: string;
}

export interface Title {
  count: number;
  title: string;
  buyLink: string;
  readLink: string;
  author: Author[];
  rating: Rating;
}

export interface Author {
  name: string;
  link?: string;
  problematic?: string;
}

export enum Rating {
  DNF = 0,
  OneStar = 1,
  TwoStars = 2,
  ThreeStars = 3,
  FourStars = 4,
  FiveStars = 5,
}
